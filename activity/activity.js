// count
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: null, totalOnSale: { $sum: 1 } } },
  {$project: {_id:0}}
]);
// greater than 20
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $match: { stock: {$gt:20} } },
  { $group: { _id: null, count: {$sum: 1}} },
  {$project: {_id:0}}
]);
// average
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", average: { $ave: "$price" } } },
]);

// maxPrice
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", maxPrice: { $max: "$price"} }  },
]);

// minPrice
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", minPrice: { $min: "$price"} }  },
]);